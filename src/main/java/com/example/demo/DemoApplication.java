package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		System.out.println("Start application");
		System.out.println(new Date());
		SpringApplication.run(DemoApplication.class, args);
	}

}
